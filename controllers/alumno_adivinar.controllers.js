const EjercicioAlumno = require("../models/alumno_adivinar.model.js");


exports.getEjercicioAlumnos = (req, res) => {

    EjercicioAlumno.getEjercicioAlumnos((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los ejercicios del alumno"
            });
        else res.send(data);
    });
};


exports.addEjercicioAlumno = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacio" });
    }

    EjercicioAlumno.addEjercicioAlumno(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la nota"
            })
        else res.status(200).send(data);
    })
};

exports.updateEjercicioAlumno = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    EjercicioAlumno.updateEjercicioAlumno(req.params.id, req.body, (err, data) => {
        console.log(req.params.id)
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el ejercicio con id : ${req.params.id}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el ejercicio con el id : " + req.params.id });
            }
        } else {
            res.status(200).send({ message: `El ejercicio se ha actualizado correctamente.` })
        }
    })
}


exports.getEjerciciosAlumno = (req, res) => {
    EjercicioAlumno.getEjerciciosAlumno(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los ejercicios"
            });
        else res.send(data);
    });
};

exports.getAdivinarNivelAlumno = (req, res) => {
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El Contenido no puede estar vacio" });
    }
    EjercicioAlumno.getAdivinarNivelAlumno(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los ejercicios"
            });
        else res.send(data);
    });
};
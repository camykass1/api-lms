const Alumnos = require("../models/alumnos.model.js");


exports.getAlumnos = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Alumnos.getAlumnos((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los alumnos"
            });
        else res.send(data);
    });
};

exports.addAlumno = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacio" });
    }

    Alumnos.addAlumno(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear el alumno"
            })
        else res.status(200).send({ message: `El alumno se ha creado correctamente` });
    })
};

exports.updateAlumno = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Alumnos.updateAlumno(req.params.id, req.body, (err, data) => {
        console.log(req.params.id)
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el alumno con id : ${req.params.id}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el alumno con el id : " + req.params.id });
            }
        } else {
            res.status(200).send({ message: `El alumno se ha actualizado correctamente.` })
        }
    })
}

exports.getAlumnoId = (req, res) => {
    Alumnos.getAlumnoId(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar el alumno"
            });
        else res.send(data);
    });
};


exports.getAlumnosFast = (req, res) => {
    Alumnos.getAlumnosFast((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los alumnos"
            });
        else res.send(data);
    });
};
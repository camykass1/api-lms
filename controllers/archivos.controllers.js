// Guardar imagenes/archivos

exports.addImagen = (req, res) => {
    let EDFile = req.files.file //EDFile.name

    EDFile.mv(`../imagenes/${EDFile.name}`, err => {
        if (err) return res.status(500).send({ message: err })
        return res.status(200).send({ message: 'File upload' })
    })
};


exports.addAudio = (req, res) => {
    console.log(req)
    let EDFile = req.files.file

    EDFile.mv(`../audios/${EDFile.name}`, err => {
        if (err) return res.status(500).send({ message: err })
        return res.status(200).send({ message: 'File upload' })
    })
};
const Completar = require("../models/completar_oraciones.model.js");


exports.getCompletar = (req, res) => {
    Completar.getCompletar((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los ejercicios"
            });
        else res.send(data);
    });
};

exports.addCompletar = (req, res) => {
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacio" });
    }

    Completar.addCompletar(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear el ejercicio"
            })
        else res.status(200).send({ message: `El ejercicio se ha creado correctamente` });
    })
};

exports.updateCompletar = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Completar.updateCompletar(req.params.id, req.body, (err, data) => {
        console.log(req.params.id)
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el ejercicio con id : ${req.params.id}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el ejercicio con el id : " + req.params.id });
            }
        } else {
            res.status(200).send({ message: `El ejercicio se ha actualizado correctamente.` })
        }
    })
}
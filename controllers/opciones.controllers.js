const Opciones = require("../models/opciones.model.js");


exports.getOpciones = (req, res) => {
    //mandamos a llamar al model y le enviamos dos parametros err=para errores 
    //y data= los datos que estamos recibiendo
    Opciones.getOpciones((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los opciones"
            });
        else res.send(data);
    });
};

exports.addOpcion = (req, res) => {
    //validamos que tenga algo el req.body
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacio" });
    }

    Opciones.addOpcion(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear la nota"
            })
        else res.status(200).send(data);
    })
};

exports.updateOpcion = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Opciones.updateOpcion(req.params.id, req.body, (err, data) => {
        console.log(req.params.id)
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro la opcion con id : ${req.params.id}` });
            } else {
                res.status(500).send({ message: "Error al actualizar la opcion con el id : " + req.params.id });
            }
        } else {
            res.status(200).send({ message: `la opcion se ha actualizado correctamente.` })
        }
    })
}


exports.getOpcionFormar = (req, res) => {
    Opciones.getOpcionFormar(req.params.id, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los ejercicios"
            });
        else res.send(data);
    });
};
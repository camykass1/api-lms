const Ordenar = require("../models/ordenar_palabras.model.js");


exports.getOrdenar = (req, res) => {
    Ordenar.getOrdenar((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los ejercicios"
            });
        else res.send(data);
    });
};

exports.addOrdenar = (req, res) => {
    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacio" });
    }

    Ordenar.addOrdenar(req.body, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Se produjo algún error al crear el ejercicio"
            })
        else res.status(200).send({ message: `El ejercicio se ha creado correctamente` });
    })
};

exports.updateOrdenar = (req, res) => {

    if (!req.body || Object.keys(req.body).length === 0) {
        res.status(400).send({ message: "El contenido no puede estar vacío" });
    }

    Ordenar.updateOrdenar(req.params.id, req.body, (err, data) => {
        console.log(req.params.id)
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({ message: `No se encontro el ejercicio con id : ${req.params.id}` });
            } else {
                res.status(500).send({ message: "Error al actualizar el ejercicio con el id : " + req.params.id });
            }
        } else {
            res.status(200).send({ message: `El ejercicio se ha actualizado correctamente.` })
        }
    })
}
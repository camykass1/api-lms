const { result } = require("lodash");
const sql = require("./db.js");


//const constructor
const AlumnoNivel = function(alumno_nivel) {};

//
AlumnoNivel.getAlumnoNivel = result => {
    sql.query(`SELECT * FROM alumno_nivel WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("alumno nivel: ", res);
        result(null, res);
    });
};

AlumnoNivel.addAlumnoNivel = (c, result) => {
    sql.query(`INSERT INTO alumno_nivel(idalumno,idnivel,fecha_inicio,fecha_fin,puntos_obtenidos,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?,?,?,?)`, [c.idalumno, c.idnivel, c.fecha_inicio, c.fecha_fin, c.puntos_obtenidos, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo, c.deleted],
        (err, res) => {
            if (err) { result(err, null); return; }
            console.log("Crear alumno nivel: ", { id: res.insertId, ...c });
            result(null, { id: res.insertId, ...c });
        }
    )
}


AlumnoNivel.updateAlumnoNivel = (id, c, result) => {
    console.log(id)
    sql.query(` UPDATE alumno_nivel SET idalumno=?,idnivel=?,fecha_inicio=?,fecha_fin=?,puntos_obtenidos=?,fecha_actualizo=?,deleted=? WHERE idalumno_nivel=?`, [c.idalumno, c.idnivel, c.fecha_inicio, c.fecha_fin, c.puntos_obtenidos, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("alumno nivel actualizado: ", { id: id, ...c });
            result(null, { id: id, ...c });
        })
}

// por alumno
AlumnoNivel.getAlumnoNiveles = (id, result) => {
    sql.query(`SELECT * FROM alumno_nivel WHERE idalumno=? AND deleted = 0`, [id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("alumno nivel: ", res);
        result(null, res);
    });
};

// por nivel join
AlumnoNivel.getAlumnosNivel = (id, result) => {
    sql.query(`SELECT * FROM alumno_nivel WHERE idnivel=? AND deleted = 0`, [id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("alumno nivel: ", res);
        result(null, res);
    });
};

module.exports = AlumnoNivel;
const { result } = require("lodash");
const sql = require("./db.js");
const sqlFast = require("./dbFAST.js");

//const constructor
const Alumnos = function(alumnos) {};

Alumnos.getAlumnos = result => {
    sql.query(`SELECT * FROM alumnos WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        var alumnos = [];
        for (const i in res) {
            var datos = {
                idalumno: res[i].idalumno,
                matricula: res[i].matricula,
                fecha_inicio: res[i].fecha_inicio,
                accessories_type: res[i].accessories_type,
                circle_color: res[i].circle_color,
                clothe_color: res[i].clothe_color,
                clothe_type: res[i].clothe_type,
                eyebrow_type: res[i].eyebrow_type,
                eye_type: res[i].eye_type,
                facial_hair_color: res[i].facial_hair_color,
                facial_hair_type: res[i].facial_hair_type,
                graphic_type: res[i].graphic_type,
                hair_color: res[i].hair_color,
                mouth_type: res[i].mouth_type,
                skin_color: res[i].skin_color,
                top_type: res[i].top_type,
                top_color: res[i].top_color,
                usuario_registro: res[i].usuario_registro,
                fecha_creacion: res[i].fecha_creacion,
                fecha_actualizo: res[i].fecha_actualizo,
                deleted: res[i].deleted,
                id: 0,
                nombre: "",
                apellido_paterno: "",
                apellido_materno: "",
                password: "",
            }

            alumnos.push(datos);
        }

        sqlFast.query(`SELECT * FROM usuarios WHERE roll=1 AND deleted = 0`, (err, res) => {
            if (err) {
                result(null, err);
                return;
            }

            for (const i in alumnos) {
                for (const j in res) {
                    if (alumnos[i].matricula == res[j].usuario) {
                        alumnos[i].nombre = res[j].nombre;
                        alumnos[i].apellido_paterno = res[j].apellido_paterno;
                        alumnos[i].apellido_materno = res[j].apellido_materno;
                        alumnos[i].password = res[j].password;
                    }
                }
            }


            console.log("alumnos: ", alumnos);
            result(null, alumnos);
        });
    });
};

Alumnos.addAlumno = (z, result) => {
    sql.query(`INSERT INTO alumnos(matricula,fecha_inicio,circle_color,accessories_type,clothe_type,clothe_color,eyebrow_type,eye_type,facial_hair_color,facial_hair_type,graphic_type,hair_color,mouth_type,skin_color,top_type,top_color,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, [z.matricula, z.fecha_inicio, z.circle_color, z.accessories_type, z.clothe_type, z.clothe_color, z.eyebrow_type, z.eye_type, z.facial_hair_color, z.facial_hair_type, z.graphic_type, z.hair_color, z.mouth_type, z.skin_color, z.top_type, z.top_color, z.usuario_registro, z.fecha_creacion, z.fecha_actualizo, z.deleted],
        (err, res) => {
            if (err) { result(err, null); return; }
            console.log("Crear alumnos: ", { id: res.insertId, ...z });
            result(null, { id: res.insertId, ...z });
        }
    )
}

Alumnos.updateAlumno = (id, z, result) => {
    sql.query(`UPDATE alumnos SET matricula=?, fecha_inicio=?, circle_color=?, accessories_type=?, clothe_type=?, clothe_color=?, eyebrow_type=?, eye_type=?, facial_hair_color=?, facial_hair_type=?, graphic_type=?, hair_color=?, mouth_type=?, skin_color=?, top_type=?, top_color=?,fecha_actualizo = ?, deleted = ? WHERE idalumno = ?`, [z.matricula, z.fecha_inicio, z.circle_color, z.accessories_type, z.clothe_type, z.clothe_color, z.eyebrow_type, z.eye_type, z.facial_hair_color, z.facial_hair_type, z.graphic_type, z.hair_color, z.mouth_type, z.skin_color, z.top_type, z.top_color, z.fecha_actualizo, z.deleted, id],
        (err, res) => {
            if (err) {
                console.log('oubo', err);
                result(null, err);
                return;
            }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }
            console.log("alumnos actualizado: ", { id: id, ...z });
            result(null, { id: id, ...z });
        })
}

Alumnos.getAlumnoId = (id, result) => {
    sql.query(`SELECT * FROM alumnos WHERE idalumno=? AND deleted = 0`, [id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("alumno: ", res);
        result(null, res);
    });
};

Alumnos.getAlumnosFast = () => {
    sqlFast.query(`SELECT * FROM usuarios WHERE roll=1 AND deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }


        console.log("alumnos: ", res);
        result(null, res);
    });
}

module.exports = Alumnos;
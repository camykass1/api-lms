const { result } = require("lodash");
const sql = require("./db.js");


//const constructor
const Niveles = function(niveles) {};

Niveles.getNiveles = result => {
    sql.query(`SELECT * FROM niveles WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("niveles: ", res);
        result(null, res);
    });
};

Niveles.addNivel = (c, result) => {
    sql.query(`INSERT INTO niveles(nivel,imagen,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?)`, [c.nivel, c.imagen, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo, c.deleted],
        (err, res) => {
            if (err) { result(err, null); return; }
            console.log("Crear niveles: ", { id: res.insertId, ...c });
            result(null, { id: res.insertId, ...c });
        }
    )
}

Niveles.updateNivel = (id, c, result) => {
    console.log(id)
    sql.query(` UPDATE niveles SET nivel=?,imagen=?,fecha_actualizo=?,deleted=? WHERE idnivel=?`, [c.nivel, c.imagen, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("Nivel actualizado: ", { id: id, ...c });
            result(null, { id: id, ...c });
        })
}

//consulta para obtener el puntaje por alumno de todos lo niveles
Niveles.getNivelAdivinarAlumno = (id, result) => {
    sql.query(`SELECT * FROM niveles WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }

        var niveles = []
        for (const i in res) {
            var datos = {
                idnivel: res[i].idnivel,
                nivel: res[i].nivel,
                imagen: res[i].imagen,
                puntos: 0,
                idalumno: 0
            }
            niveles.push(datos)
        }

        sql.query(`SELECT * FROM alumno_adivinar WHERE deleted = 0 AND idalumno=?`, [id], (err, res) => {
            if (err) {
                result(null, err);
                return;
            }

            for (const i in niveles) {
                var cont = 0;
                for (const j in res) {
                    if (niveles[i].idnivel == res[j].idnivel) {
                        cont = cont + res[j].puntos_obtenidos;
                    }
                }
                niveles[i].idalumno = id;
                niveles[i].puntos = cont;
            }


            console.log("niveles: ", niveles);
            result(null, niveles);
        });
    });
};

module.exports = Niveles;
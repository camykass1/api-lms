const { result } = require("lodash");
const sql = require("./db.js");


//const constructor
const Opciones = function(opciones) {};

Opciones.getOpciones = result => {
    sql.query(`SELECT * FROM opciones WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("opciones: ", res);
        result(null, res);
    });
};

Opciones.addOpcion = (c, result) => {
    sql.query(`INSERT INTO opciones(opcion,idformar,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?)`, [c.opcion, c.idformar, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo, c.deleted],
        (err, res) => {
            if (err) { result(err, null); return; }
            console.log("Crear opciones: ", { id: res.insertId, ...c });
            result(null, { id: res.insertId, ...c });
        }
    )
}

Opciones.updateOpcion = (id, c, result) => {
    console.log(id)
    sql.query(` UPDATE opciones SET opcion=?,idformar=?,fecha_actualizo=?,deleted=? WHERE idopcion=?`, [c.opcion, c.idformar, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("Opcion actualizada: ", { id: id, ...c });
            result(null, { id: id, ...c });
        })
}

Opciones.getOpcionFormar = (id, result) => {
    sql.query(`SELECT * FROM opciones WHERE idformar=? AND deleted = 0`, [id], (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("ejercicios: ", res);
        result(null, res);
    });
};

module.exports = Opciones;
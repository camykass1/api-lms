const { result } = require("lodash");
const sql = require("./db.js");


//const constructor
const Tipos = function(tipo_ejercicios) {};

Tipos.getTipos = result => {
    sql.query(`SELECT * FROM tipo_ejercicios WHERE deleted = 0`, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }
        console.log("tipo_ejercicios: ", res);
        result(null, res);
    });
};

Tipos.addTipo = (c, result) => {
    sql.query(`INSERT INTO tipo_ejercicios(tipo_ejercicio,imagen,usuario_registro,fecha_creacion,fecha_actualizo,deleted)VALUES(?,?,?,?,?,?)`, [c.tipo_ejercicio, c.imagen, c.usuario_registro, c.fecha_creacion, c.fecha_actualizo, c.deleted],
        (err, res) => {
            if (err) { result(err, null); return; }
            console.log("Crear tipo_ejercicios: ", { id: res.insertId, ...c });
            result(null, { id: res.insertId, ...c });
        }
    )
}

Tipos.updateTipo = (id, c, result) => {
    console.log(id)
    sql.query(` UPDATE tipo_ejercicios SET tipo_ejercicio=?,imagen=?,fecha_actualizo=?,deleted=? WHERE idtipo_ejercicio=?`, [c.tipo_ejercicio, c.imagen, c.fecha_actualizo, c.deleted, id],
        (err, res) => {
            if (err) { result(null, err); return; }
            if (res.affectedRows == 0) {
                result({ kind: "not_found" }, null);
                return;
            }

            console.log("Nivel actualizado: ", { id: id, ...c });
            result(null, { id: id, ...c });
        })
}



module.exports = Tipos;
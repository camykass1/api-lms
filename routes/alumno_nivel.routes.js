module.exports = app => {
    const alumno_nivel = require('../controllers/alumno_nivel.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/alumno_nivel.all", alumno_nivel.getAlumnoNivel); // OBTENER TODOS LOS EJERCICIOS
    app.post("/alumno_nivel.add", alumno_nivel.addAlumnoNivel); //AGREGAR 
    app.put("/alumno_nivel.update/:id", alumno_nivel.updateAlumnoNivel); //actualizar 
    app.get("/alumno_nivel.alumno/:id", alumno_nivel.getAlumnoNiveles); // OBTENER TODOS LOS NIVELES DE UN ALUMNO
    app.get("/alumno_nivel.nivel/:id", alumno_nivel.getAlumnosNivel); // OBTENER TODOS LOS ALUMNOS DE UN NIVEL

};
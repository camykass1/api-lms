module.exports = app => {
    const alumno_ordenar = require('../controllers/alumno_ordenar.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/alumno_ordenar.all", alumno_ordenar.getEjercicioAlumnos); // OBTENER TODOS LOS EJERCICIOS
    app.post("/alumno_ordenar.add", alumno_ordenar.addEjercicioAlumno); //AGREGAR 
    app.put("/alumno_ordenar.update/:id", alumno_ordenar.updateEjercicioAlumno); //actualizar 
    app.get("/alumno_ordenar.alumno/:id", alumno_ordenar.getEjerciciosAlumno); // OBTENER TODOS LOS EJERCICIOS DE UN ALUMNO

};
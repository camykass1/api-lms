module.exports = app => {
    const alumnos = require('../controllers/alumnos.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/alumnos.all", alumnos.getAlumnos); // OBTENER
    app.get("/alumnos.id/:id", alumnos.getAlumnoId); // OBTENER
    app.post("/alumnos.add", alumnos.addAlumno); //AGREGAR 
    app.put("/alumnos.update/:id", alumnos.updateAlumno); //actualizar 

    app.get("/alumnos.fast.all", alumnos.getAlumnosFast); // OBTENER
};
module.exports = app => {
    const completar_oraciones = require('../controllers/completar_oraciones.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/completar_oraciones.all", completar_oraciones.getCompletar); // OBTENER TODOS LOS EJERCICIOS
    app.post("/completar_oraciones.add", completar_oraciones.addCompletar); //AGREGAR 
    app.put("/completar_oraciones.update/:id", completar_oraciones.updateCompletar); //actualizar 
};
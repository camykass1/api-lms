module.exports = app => {
    const niveles = require('../controllers/niveles.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/niveles.all", niveles.getNiveles); // OBTENER
    app.get("/niveles.adivinar.alumno/:id", niveles.getNivelAdivinarAlumno); // OBTENER EL PUNTAJE DE CADA ALUMNO DE TODOS LOS NIVELES
    app.post("/niveles.add", niveles.addNivel); //AGREGAR 
    app.put("/niveles.update/:id", niveles.updateNivel); //actualizar 

};
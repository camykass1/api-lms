module.exports = app => {
    const opciones = require('../controllers/opciones.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/opciones.all", opciones.getOpciones); // OBTENER
    app.get("/opciones.formar/:id", opciones.getOpcionFormar); // OBTENER
    app.post("/opciones.add", opciones.addOpcion); //AGREGAR 
    app.put("/opciones.update/:id", opciones.updateOpcion); //actualizar 

};
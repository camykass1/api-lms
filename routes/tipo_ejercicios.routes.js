module.exports = app => {
    const tipo_ejercicios = require('../controllers/tipo_ejercicios.controllers') // const para utilizar nuestro controllador--> ADDED THIS
    app.get("/tipo_ejercicios.all", tipo_ejercicios.getTipos); // OBTENER
    app.post("/tipo_ejercicios.add", tipo_ejercicios.addTipo); //AGREGAR 
    app.put("/tipo_ejercicios.update/:id", tipo_ejercicios.updateTipo); //actualizar 

};
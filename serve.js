// IMPORTAR DEPENDENCIAS
const express = require("express");
const bodyParser = require("body-parser");
var cors = require('cors');
const fileUpload = require('express-fileupload');
// IMPORTAR EXPRESS
const app = express();

//Para servidor con ssl
var http = require('http');
const https = require('https');
const fs = require('fs');

// Rutas estaticas
app.use("/imagen", express.static("../imagenes"));
app.use('/audio', express.static('../audios'));

// IMPORTAR PERMISOS
app.use(cors());
// parse requests of content-type: application/json
app.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload()); //subir archivos


// ----IMPORTAR RUTAS---------------------------------------->
var r_niveles = require('./routes/niveles.routes');
r_niveles(app);

var r_alumno_nivel = require('./routes/alumno_nivel.routes');
r_alumno_nivel(app);


var r_adivinar = require('./routes/adivinar_palabras.routes');
r_adivinar(app);

var r_alumno_adivinar = require('./routes/alumno_adivinar.routes');
r_alumno_adivinar(app);

var r_completar_oraciones = require('./routes/completar_oraciones.routes');
r_completar_oraciones(app);

var r_alumno_completar = require('./routes/alumno_completar.routes');
r_alumno_completar(app);

var r_formar_oraciones = require('./routes/formar_oraciones.routes');
r_formar_oraciones(app);

var r_alumno_formar = require('./routes/alumno_formar.routes');
r_alumno_formar(app);


var r_ordenar_palabras = require('./routes/ordenar_palabras.routes');
r_ordenar_palabras(app);

var r_alumno_ordenar = require('./routes/alumno_ordenar.routes');
r_alumno_ordenar(app);


var r_alumnos = require('./routes/alumnos.routes');
r_alumnos(app);

var r_opciones = require('./routes/opciones.routes');
r_opciones(app);


var r_tipo_ejercicios = require('./routes/tipo_ejercicios.routes');
r_tipo_ejercicios(app);


var r_archivos = require("./routes/archivos.routes");
r_archivos(app);

// ----FIN-DE-LAS-RUTAS-------------------------------------->

// DEFINIT PUERTO EN EL QUE SE ESCUCHARA
app.listen(3008, () => {
    console.log("|****************** S-O-F-S-O-L-U-T-I-O-N *********************| ");
    console.log("|******************** C-R-E-A-T-I-B-E *************************| ");
    console.log("|**************************************************************| ");
    console.log("|************ Servidor Corriendo en el Puerto 3008 ************| ");
});


// //Para servidor con ssl

// https.createServer({
//     key: fs.readFileSync('/etc/letsencrypt/live/sofsolution.com/privkey.pem'),
//     cert: fs.readFileSync('/etc/letsencrypt/live/sofsolution.com/fullchain.pem'),
//     passphrase: 'desarrollo'
// }, app)
// .listen(3003);